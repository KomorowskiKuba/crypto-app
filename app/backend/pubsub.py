from pubnub.pubnub import PubNub
from pubnub.pnconfiguration import PNConfiguration
from pubnub.callbacks import SubscribeCallback

from blockchain.block import Block
from wallet.transaction import Transaction

pn_config = PNConfiguration()
pn_config.subscribe_key = 'sub-c-c9751de6-a2cc-11ec-81c7-420d26494bdd'
pn_config.publish_key = 'pub-c-7166264b-c4d2-4cab-9a13-c39f73ac47db'
pn_config.uuid = 'Komorowski-Kuba'

pubnub = PubNub(pn_config)

CHANNELS = {
    'TEST': 'TEST',
    'BLOCK': 'BLOCK',
    'TRANSACTION': 'TRANSACTION'
}


class Listener(SubscribeCallback):
    def __init__(self, blockchain, transaction_pool):
        self.blockchain = blockchain
        self.transaction_pool = transaction_pool

    def message(self, pn, message_object):
        print(f'\n-- Channel: {message_object.channel}, incoming message: {message_object.message}')

        if message_object.channel == CHANNELS['BLOCK']:
            block = Block.from_json(message_object.message)
            new_chain = self.blockchain.chain[:]
            new_chain.append(block)

            try:
                self.blockchain.replace_chain(new_chain)
                self.transaction_pool.clear_blockchain_transactions(self.blockchain)
                print('Successfully replaced the local chain!')
            except Exception as e:
                print(f'Did not replace the chain. {e}')

        elif message_object.channel == CHANNELS['TRANSACTION']:
            transaction = Transaction.from_json(message_object.message)
            self.transaction_pool.set_transaction(transaction)
            print('\n -- Set the new transaction in pool!')


class PubSub:
    def __init__(self, blockchain, transaction_pool):
        self.pubnub = PubNub(config=pn_config)
        self.pubnub.subscribe().channels(CHANNELS.values()).execute()
        self.pubnub.add_listener(Listener(blockchain, transaction_pool))

    def publish(self, channel, message):
        self.pubnub.publish().channel(channel).message(message).sync()

    def broadcast_block(self, block):
        self.publish(CHANNELS['BLOCK'], block.to_json())

    def broadcast_transaction(self, transaction):
        self.publish(CHANNELS['TRANSACTION'], transaction.to_json())
