def hex_to_binary(hex_string):
    bin_number = bin(int(hex_string, base=16))[2:].zfill(4 * len(hex_string))
    return str(bin_number)
