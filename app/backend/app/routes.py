import sys
import requests
import os
import random

from pubsub import PubSub
from flask import Flask, jsonify, request

from blockchain.blockchain import Blockchain
from wallet.transaction import Transaction
from wallet.transaction_pool import TransactionPool
from wallet.wallet import Wallet
from flask_cors import CORS

app = Flask(__name__)
CORS(app,resources={r'/*': {'origins':'http://localhost:3000'}})

blockchain = Blockchain()
wallet = Wallet(blockchain)
transaction_pool = TransactionPool()
pubsub = PubSub(blockchain, transaction_pool)

SEED_DATA=True


@app.route('/home', methods=['GET'])
def route_home():
    return 'Welcome!'


@app.route('/blockchain', methods=['GET'])
def route_blockchain():
    return jsonify(blockchain.to_json())

@app.route('/blockchain/range')
def route_blockchain_range():
    # http://localhost:5000/blockchain/range?start=2&end=5
    start = int(request.args.get('start'))
    end = int(request.args.get('end'))

    return jsonify(blockchain.to_json()[::-1][start:end])

@app.route('/blockchain/length')
def route_blockchain_length():
    return jsonify(len(blockchain.chain))


@app.route('/blockchain/mine', methods=['GET'])
def route_blockchain_mine():
    transaction_data = transaction_pool.transaction_data()
    transaction_data.append(Transaction.reward_transaction(wallet).to_json())
    blockchain.add_block(transaction_data)

    block = blockchain.chain[-1]
    pubsub.broadcast_block(block)

    transaction_pool.clear_blockchain_transactions(blockchain)

    return jsonify(block.to_json())


@app.route('/wallet/transact', methods=['POST'])
def route_wallet_transact():
    transaction_data = request.get_json()
    transaction = transaction_pool.existing_transaction(wallet.address)

    if transaction:
        transaction.update(wallet, transaction_data['recipient'], transaction_data['amount'])
    else:
        transaction = Transaction(wallet, transaction_data['recipient'], transaction_data['amount'])

    pubsub.broadcast_transaction(transaction)

    return jsonify(transaction.to_json())


@app.route('/wallet/info')
def route_wallet_info():
    return jsonify({
        'address': wallet.address,
        'balance': wallet.balance
    })

@app.route('/transactions')
def route_transactions():
    return jsonify(transaction_pool.transaction_data())


ROOT_PORT = 5000
PORT = ROOT_PORT




#for i in range(10):
#    blockchain.add_block([
#        Transaction(Wallet(), Wallet().address, random.randint(2, 50)).to_json(),
#        Transaction(Wallet(), Wallet().address, random.randint(2, 50)).to_json(),
#        Transaction(Wallet(), Wallet().address, random.randint(2, 50)).to_json()
#    ])

#for i in range(3):
#        transaction_pool.set_transaction(
#            Transaction(Wallet(), Wallet().address, random.randint(2, 50))
#        )


if len(sys.argv) > 1:
    PORT = sys.argv[1]

    url = f'http://localhost:{ROOT_PORT}/blockchain'

    result = requests.get(url)
    result_blockchain = Blockchain.from_json(result.json())

    try:
        blockchain.replace_chain(result_blockchain.chain)
        print('Successfully synchronized blockchain!')

    except Exception:
        print('Error synchronizing blockchain!')

app.run(debug=True, port=PORT)
