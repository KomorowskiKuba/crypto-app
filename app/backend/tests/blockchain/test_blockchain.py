import pytest

from blockchain.blockchain import Blockchain
from blockchain.block import GENESIS_DATA
from wallet.transaction import Transaction
from wallet.wallet import Wallet


def test_blockchain_instance():
    blockchain = Blockchain()
    assert blockchain.chain[0].hash == GENESIS_DATA['hash']


def test_add_block():
    blockchain = Blockchain()
    data = 'test-data'
    blockchain.add_block(data)

    assert blockchain.chain[-1].data == data


@pytest.fixture
def blockchain_generate_blocks():
    blockchain = Blockchain()

    for i in range(3):
        blockchain.add_block([
            Transaction(Wallet(), 'recipient', i).to_json()
        ])

    return blockchain


def test_is_transaction_chain_valid(blockchain_generate_blocks):
    Blockchain.is_valid_transaction_chain(blockchain_generate_blocks.chain)


def test_is_transaction_chain_valid_duplicate_transactions(blockchain_generate_blocks):
    transaction = Transaction(Wallet(), 'recipient', 1).to_json()
    blockchain_generate_blocks.add_block([transaction, transaction])

    with pytest.raises(Exception, match='is not unique'):
        Blockchain.is_valid_transaction_chain(blockchain_generate_blocks.chain)


def test_is_transaction_chain_valid_multiple_rewards(blockchain_generate_blocks):
    reward_1 = Transaction.reward_transaction(Wallet()).to_json()
    reward_2 = Transaction.reward_transaction(Wallet()).to_json()

    blockchain_generate_blocks.add_block([reward_1, reward_2])

    with pytest.raises(Exception, match='one mining reward per block'):
        Blockchain.is_valid_transaction_chain(blockchain_generate_blocks.chain)


def test_is_transaction_chain_valid_bad_transaction(blockchain_generate_blocks):
    bad_transaction = Transaction(Wallet(), 'recipient', 1)
    bad_transaction.input['signature'] = Wallet().sign(bad_transaction.output)
    blockchain_generate_blocks.add_block([bad_transaction.to_json()])

    with pytest.raises(Exception):
        Blockchain.is_valid_transaction_chain(blockchain_generate_blocks.chain)


def test_is_transaction_chain_valid_bad_historic_balance(blockchain_generate_blocks):
    wallet = Wallet()
    bad_transaction = Transaction(wallet, 'recipient', 1)
    bad_transaction.output[wallet.address] = 10_000
    bad_transaction.input['amount'] = 9001
    bad_transaction.input['signature'] = wallet.sign(bad_transaction.output)

    blockchain_generate_blocks.add_block([bad_transaction.to_json()])

    with pytest.raises(Exception, match='has invalid input amount!'):
        Blockchain.is_valid_transaction_chain(blockchain_generate_blocks.chain)