import React, {useState} from 'react';
import {FormGroup,FormControl ,Button,Form} from 'react-bootstrap'
import {URL} from '../config';

function DoTransaction(){
    const [recipient,setRecipient] = useState('');
    const [amount,setAmount] = useState(0);

    const updateRecipient=event => {
        setRecipient(event.target.value);
    }
    const updateAmount=event => {
        setAmount(Number(event.target.value));
    }

    const submitTran=() => {
        fetch(`${URL}/wallet/transact`,
        {
        method: 'POST',
        headers:{'Content-Type': 'application/json'},
        body: JSON.stringify({recipient,amount})
        }).then(response =>response.json()).then(json =>{
            console.log('submitTran json',json);
            alert('Wysłane ;)')
        });
    }
    return(
        <div className="doTransaction">
            <h3>Wyślij</h3>
            <br />
            <FormGroup>
            <Form.Label>Odbiorca</Form.Label>
                <FormControl
                class="form-control"
                input='text'
                placeholder=""
                value={recipient}
                onChange={updateRecipient}
                />
            </FormGroup>
            <FormGroup>
                <Form.Label>Ilość</Form.Label>
                <FormControl
                className="form-control"
                input="number"
                placeholder="ilosc"
                value={amount}
                onChange={updateAmount}
                />
            </FormGroup>
            <div>
                <Button
                className="btn-big"
                variant="info"
                onClick={submitTran}
                >
                    Wyślij
                </Button>
            </div>
        </div>
    )
}

export default DoTransaction
