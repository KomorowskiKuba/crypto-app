import React, { useState, useEffect } from 'react';
import Block from '../components/Block';
import { URL, MILISEC } from '../config';
import './Table.css'

function History() {
  const [walletInfo, setWalletInfo] = useState({});
  const [blockchain, setBlockchain] = useState([]);

  useEffect(() => {
    fetch(`${URL}/wallet/info`)
      .then(response => response.json())
      .then(json => setWalletInfo(json))
  }, []);

  const fetchBlockchain = () => {
    fetch(`${URL}/blockchain`)
      .then(response => response.json())
      .then(json => setBlockchain(json));
  };

  useEffect(() => {
    fetchBlockchain();
  }, []);

  

  const walletAddress = walletInfo.address;

  return (
    <div className='History'>
      <h3>Historia portfela o adresie:</h3>
      <h4>{walletAddress}</h4>
        <div>
          <table class="content-table">
            <thead>
              <tr>
                <th>Id</th>
                <th>Adres nadawcy</th>
                <th>Adres odbiorcy</th>
                <th>Wartosc</th>
                <th>Data</th>
              </tr>
            </thead>
            <tbody>
              {
              blockchain.filter(block => {
                return (
                  block.data.length > 0 &&
                  (block.data[0].input.address == walletAddress ||
                    Object.keys(block.data[0].output) == walletAddress)
                )
              }).map(block => {
                return <tr key={block.hash}>
                  <td>{block.data[0].id}</td>
                  <td>{block.data[0].input.address}</td>
                  <td>{Object.keys(block.data[0].output)}</td>
                  <td>{Object.values(block.data[0].output)}</td>
                  <td>{new Date(block.timestamp / MILISEC).toLocaleString()}</td>
                </tr>
              })
              }
            </tbody>
          </table>
        </div>   
             
    </div>
  )
}

export default History;