import React from 'react'
import {Button} from 'react-bootstrap'
import { URL } from '../config'
import logo from '../assets/miner.gif'

const fetchMineBlock = () => {
  fetch(`${URL}/blockchain/mine`)
  .then(()=> {
    alert('Wykopane :)')
  }
  );
}

function Mine() {
  return (
    <div className='mine'>
        <h1>Kopalnia</h1>
        <img src={logo} alt="loading..." />
        <div>
        <Button
        className="btn-big"
        variant="info"
        onClick={fetchMineBlock}>
          Wykop
        </Button>
        </div>
    </div>
  );
}

export default Mine