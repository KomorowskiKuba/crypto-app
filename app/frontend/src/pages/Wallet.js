import React, { useState, useEffect } from 'react'
import logo from '../assets/logo2.png';
import { URL } from '../config';

function Wallet() {

  const [walletInfo, setWalletInfo ] = useState({});

  useEffect(() => {
    fetch(`${URL}/wallet/info`)
      .then(response => response.json())
      .then(json => setWalletInfo(json));
  }, []);

  const { address, balance } = walletInfo;

  return (
    <div className='wallet'>
      <img className="logo" src={logo} alt="application-logo" />
      <h3>Elektryczny pjesek moon coin</h3>

      <br />
      <div className="WalletInfo">
        <div>Adres: {address}</div>
        <div>Balans: {balance}</div>
      </div>
    </div>
  );
}

export default Wallet