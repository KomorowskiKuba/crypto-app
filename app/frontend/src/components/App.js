import  React from 'react';
import Navbar from './Navbar';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Wallet from '../pages/Wallet'
import Mine from '../pages/Mine'
import Blockchain from '../pages/Blockchain';
import DoTransaction from '../pages/DoTransaction'
import History from '../pages/History';

function App() {

  return (
    <div className="App">
      <Router>
        <Navbar />
        <Routes>
          <Route path='/' exact element={<Blockchain />} />
            <Route path='/wallet' exact element={<Wallet />} />
            <Route path='/transaction' element={<DoTransaction />} />
            <Route path='/mine' element={<Mine />} />
            <Route path='/history' element={<History />} />
        </Routes>
      </Router>  
    </div>
  );
}

export default App;