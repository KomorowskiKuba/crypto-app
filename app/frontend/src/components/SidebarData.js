import React from 'react';
import { FaWallet } from 'react-icons/fa'
import { RiSendPlaneFill } from 'react-icons/ri'
import { SiHiveBlockchain } from 'react-icons/si'
import { GiMining } from 'react-icons/gi'
import { FaHistory } from 'react-icons/fa';

export const SidebarData = [
  {
    title: 'Blockchain',
    path: '/',
    icon:  <SiHiveBlockchain />,
    className: 'nav-text'
  },
  {
    title: 'Nowa transakcja',
    path: '/transaction',
    icon: <RiSendPlaneFill />,
    className: 'nav-text'
  },
  {
    title: 'Portfel',
    path: '/wallet',
    icon: <FaWallet />,
    className: 'nav-text'
  },
  {
    title: 'Kopanie',
    path: '/mine',
    icon: <GiMining />,
    className: 'nav-text'
  },
  {
    title: 'Historia transakcji',
    path: '/history',
    icon: <FaHistory />,
    className: 'nav-text'
  }
]

